const reset = document.getElementById("reset");
reset.addEventListener("click", function () {
  location.reload();
});

const buttonRestart = () => {
  document.getElementById("popUp").style.display = "none";
  document.getElementById("glass").style.display = "none";
  document.getElementById("popUpWin").style.display = "none";
  location.reload();
};
const buttonReset = document.getElementById("restartButton");
buttonReset.addEventListener("click", buttonRestart);
