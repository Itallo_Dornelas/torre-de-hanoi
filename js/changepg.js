function User() {
  const nome = document.getElementById("nome").value;
  const pla = document.getElementById("player");
  if (nome === "") {
    document.querySelector("#player").style.display = " none";
  } else {
    pla.innerText = `${nome}`;
  }
}

function changePage() {
  const pg1 = document.getElementById("pg1");
  const pg2 = document.getElementById("pg2");
  pg1.disabled = true;
  pg2.disabled = false;
  document.querySelector("#change").style.display = " none";
  document.querySelector("#form").style.display = " none";
  User();
}
const change = document.getElementById("change");
let select = document.getElementById("select");
let selectValue = select.options[select.selectedIndex].value;

change.addEventListener("click", changePage, creatTower());

function creatTower() {
  const toBase = document.getElementById("base");
  for (let i = 1; i <= 3; i++) {
    let creatingTower = document.createElement("div");
    creatingTower.id = `tower${i}`;
    creatingTower.classList = "tower";
    toBase.appendChild(creatingTower);
  }
  creatDisk(4);
}

function creatDisk(value) {
  const toTower = document.getElementById("tower1");
  for (let i = 1; i <= value; i++) {
    let creatingDisk = document.createElement("div");
    creatingDisk.id = `disk${i}`;
    toTower.appendChild(creatingDisk);
  }
}
