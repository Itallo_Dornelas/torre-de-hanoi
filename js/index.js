let selector = null;
let diskWidth = null;
let notMove = null;
addEventListener("click", selecionaClick);
function selecionaClick(event) {
  const elemento = event.target;
  diskOrTower(elemento);
}
function diskOrTower(elemento) {
  const disk = elemento.id.indexOf("disk") === 0;
  const towers = elemento.id.indexOf("tower") === 0;
  if (disk) {
    moveDisk(elemento);
  } else if (towers) {
    toTower(elemento);
  }
}
function toTower(elemento) {
  if (selector) {
    const arrayElements = [...elemento.children];

    for (let i = 0; i < arrayElements.length; i++) {
      if (arrayElements[i].clientWidth < diskWidth) {
        notMove = arrayElements[i];
      } else {
        notMove = null;
      }
    }
    if (!notMove) {
      elemento.prepend(selector);
    } else {
      alert("Movimento Invalido");
    }
  }
  win();
}

function moveDisk(elemento) {
  const torre = elemento.parentNode;
  const arrayFilhos = [...torre.children];
  if (arrayFilhos[0] === elemento) {
    diskWidth = elemento.clientWidth;
    selector = elemento;
  } else {
    alert("Movimento Invalido");
  }
}
function win() {
  const torre = document.getElementById("tower3");
  const arrayTorre = [...torre.children];
  if (arrayTorre.length === 4) {
    popUpWin("Voce Ganhou!");
  }
}
